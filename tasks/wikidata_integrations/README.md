# [Wikidata Integrations](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/tasks/wikidata_integrations)

Analytics tasks for the Wikidata Integrations team organized by the year the task was finished.
