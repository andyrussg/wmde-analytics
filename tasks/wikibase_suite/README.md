# [Wikibase Suite](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/tasks/wikibase_suite)

Analytics tasks for the Wikibase Suite team organized by the year the task was finished.
