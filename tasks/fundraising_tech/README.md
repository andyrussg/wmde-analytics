# [Fundraising Tech](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/tasks/fundraising_tech)

Analytics tasks for the Fundraising Tech team organized by the year the task was finished.
