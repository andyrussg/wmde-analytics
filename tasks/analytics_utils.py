"""
Utility functions for accessing and working with data.

Contents:
    display_peak,
    print_spark_session_info,
    get_plot_axis_png_title,
    add_webrequest_date_where_clause,
    get_wdid_label
"""

from datetime import date, timedelta
import os
from urllib.error import HTTPError

from IPython.display import clear_output, display
import pandas as pd
from pyspark.sql.session import SparkSession

try:
    from SPARQLWrapper import JSON, POST, SPARQLWrapper
except ImportError:
    os.system("pip install sparqlwrapper")
    from SPARQLWrapper import JSON, POST, SPARQLWrapper


def display_peak(df: pd.DataFrame, rows: int = 5, skip: bool = False):
    """
    Displays a pandas DataFrame and waits for the user to respond and clear it.

    Parameters
    ----------
        df : pd.DataFrame
            The DataFrame to be displayed.

        rows : int
            How many rows should be shown.

        skip : bool (default=False)
            Whether to skip the display in cases where it's not needed anymore.

    Returns
        The display or a dataframe and a prompt to ask if the user is ready to clear it.
    """
    if skip is False:
        current_max_rows = pd.get_option("display.max_rows")
        pd.set_option("display.max_rows", rows)

        display(df.head(rows))

        pd.set_option("display.max_rows", current_max_rows)

        input("Press return to clear the output:")
        clear_output()
        print("\nDisplay of DataFrame cleared.\n")

    else:
        print("Display of DataFrame skipped.")


def print_spark_session_info(spark: SparkSession):
    """
    Prints helpful information about a wmfdata PySpark session for documentation and monitoring.
    """
    print(f"Spark version: {spark.sparkContext.version}")
    print(f"Spark app name: {spark.sparkContext.appName}")
    print(
        f"Scheduler UI: https://yarn.wikimedia.org/proxy/{spark.sparkContext.applicationId}"
    )


def get_plot_axis_png_title(ax, directory: str = ""):
    """
    Returns a title of a PNG plot given its title via the axis.
    """
    if not directory:
        directory = "."

    return (
        f"{directory}/"
        + ax.get_title()
        .split("\n")[0]
        .lower()
        .replace(" ", "_")
        .replace("(", "")
        .replace(")", "")
        + ".png"
    )


def add_webrequest_date_where_clause(
    query: str, inclusive_start_date: str, inclusive_end_date: str
):
    """
    Adds a standardized WHERE clause to queries. This function is meant for the wmf.webrequest table.

    Parameters
    ----------
        query : str
            The query for which a WHERE clause should be added.

        inclusive_start_date : str
            The date from which the WHERE clause should begin.

        inclusive_end_date : str
            The last date that should be included in the WHERE clause.

    Returns
    -------
        query_with_date_where : str
            An sql query string that includes a WHERE clause given the date kwargs passed.
    """
    query_with_date_where = ""

    # Dates are present, so add subsets based on them into the WHERE clause.
    start_year = inclusive_start_date.split("-")[0]
    end_year = inclusive_end_date.split("-")[0]
    start_month = inclusive_start_date.split("-")[1]
    end_month = inclusive_end_date.split("-")[1]
    start_day = inclusive_start_date.split("-")[2]
    end_day = inclusive_end_date.split("-")[2]

    inclusive_end_date_next_day = (
        date(int(end_year), int(end_month), int(end_day)) + timedelta(1)
    ).strftime("%Y-%m-%d")

    if "WHERE" in query:
        # There already is a WHERE clause, so add the date subsets to the top of the WHERE clause.
        query_with_date_where = query.split("WHERE")[0]
        query_with_date_where += "WHERE"

        if start_year != end_year:
            query_with_date_where += f"""
    '{inclusive_start_date}' <= dt
    AND dt < '{inclusive_end_date_next_day}'
    AND {query.split("WHERE")[1][5:]}
    """

        else:
            if start_month != end_month:
                query_with_date_where += f"""
    year = {int(start_year)}
    AND '{inclusive_start_date}' <= dt
    AND dt < '{inclusive_end_date_next_day}'
    AND {query.split("WHERE")[1][5:]}
    """

            else:
                if start_day != end_day:
                    query_with_date_where += f"""
    year = {int(start_year)}
    AND month = {int(start_month)}
    AND '{inclusive_start_date}' <= dt
    AND dt < '{inclusive_end_date_next_day}'
    AND {query.split("WHERE")[1][5:]}
    """

                else:
                    query_with_date_where += f"""
    year = {int(start_year)}
    AND month = {int(start_month)}
    AND day = {int(start_day)}
    AND {query.split("WHERE")[1][5:]}
    """

    else:
        # There is no WHERE clause, so add one and include the rest of the query at the end.
        split_keyword = ""
        end_of_query = ""

        if "GROUP BY" in query:
            split_keyword = "GROUP BY"
        elif "HAVING" in query:
            split_keyword = "HAVING"
        elif "ORDER BY" in query:
            split_keyword = "ORDER BY"
        elif "LIMIT" in query:
            split_keyword = "LIMIT"

        if split_keyword != "":
            query_with_date_where = query.split(split_keyword)[0]
            query_with_date_where += "WHERE"
            end_of_query = query.split(split_keyword)[1][5:]

        else:
            query_with_date_where = query
            query_with_date_where += "\nWHERE"

        if start_year != end_year:
            query_with_date_where += f"""
    '{inclusive_start_date}' <= dt
    AND dt < '{inclusive_end_date_next_day}'
    """

        else:
            if start_month != end_month:
                query_with_date_where += f"""
    year = {int(start_year)}
    AND '{inclusive_start_date}' <= dt
    AND dt < '{inclusive_end_date_next_day}'
    """

            else:
                if start_day != end_day:
                    query_with_date_where += f"""
    year = {int(start_year)}
    AND month = {int(start_month)}
    AND '{inclusive_start_date}' <= dt
    AND dt < '{inclusive_end_date_next_day}'
    """

                else:
                    query_with_date_where += f"""
    year = {int(start_year)}
    AND month = {int(start_month)}
    AND day = {int(start_day)}
    """

        # Add the rest of the query back into the end, if needed.
        if split_keyword != "":
            query_with_date_where = query_with_date_where.strip()
            query_with_date_where += f"\n\n{split_keyword}"
            query_with_date_where += f"\n    {end_of_query}"

    print(query_with_date_where)

    return query_with_date_where


def get_wdid_label(wd_id: str):
    """
    Returns the label of a provided Wikidata entity ID.

    Parameters
    ----------
        wd_id : str
            A QID, PID or LID for a Wikidata entity.

    Returns
    -------
        label : str
            The label for the provided Wikidata entity.
    """
    query = ""
    if wd_id[0] in ["P", "Q"]:
        query = f"""
        SELECT  *
        WHERE {{
            wd:{wd_id} rdfs:label ?label .
            FILTER (langMatches(lang(?label), "EN"))
        }}
        """

    elif wd_id[0] == "L":
        query = f"""
        SELECT ?lemma
        WHERE {{
            wd:{wd_id} a ontolex:LexicalEntry;
                wikibase:lemma ?lemma .
        }}
        """

    else:
        raise ValueError("Please pass a valid Wikidata QID, PID or LID.")

    sparql = SPARQLWrapper("https://query.wikidata.org/sparql")
    sparql.setReturnFormat(JSON)
    sparql.setMethod(POST)

    sparql.setQuery(query)

    results = None
    try:
        results = sparql.query().convert()
    except HTTPError as err:
        print(
            f"There was an HTTPError when trying to return the label for {wd_id}. Please try again!"
        )
        print(f"Error: {err}")

    if results is None:
        print("Nothing returned by the WDQS server for the given Wikidata ID.")
        return

    else:
        # Subset the returned JSON and the individual results before saving.
        query_results = results["results"]["bindings"][0]

    if wd_id[0] in ["P", "Q"]:
        label = query_results["label"]["value"]
    else:
        label = query_results["lemma"]["value"]

    return label
