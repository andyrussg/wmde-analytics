# [Tasks](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/tasks)

Research and tasks by Analytics at Wikimedia Deutschland. Work is organized by team and the year the task was finished.

### Contents

- [Fundraising Tech](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/tasks/fundraising_tech)
  - Analytics work for the Fundraising Tech team
- [Product Platform](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/tasks/product_platform)
  - Analytics work for the Product Platform team
- [Technical Wishes](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/tasks/technical_wishes)
  - Analytics work for the Technical Wishes team
- [Wikibase Cloud](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/tasks/wikibase_cloud)
  - Analytics work for the Wikibase Cloud team
- [Wikibase Suite](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/tasks/wikibase_suite)
  - Analytics work for the Wikibase Suite team
- [Wikidata](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/tasks/wikidata)
  - Analytics work for the Wikidata team
- [Wikidata Integrations](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/tasks/wikidata_integrations)
  - Analytics work for the Wikidata Integrations team
- [analytics_utils.py](https://gitlab.wikimedia.org/repos/wmde/analytics/-/blob/main/tasks/analytics_utils.py)
  - Utility functions for accessing and working with data
- [task_template.ipynb](https://gitlab.wikimedia.org/repos/wmde/analytics/-/blob/main/tasks/task_template.ipynb)
  - A template notebook to start tasks
