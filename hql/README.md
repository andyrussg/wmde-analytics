# [HQL](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/hql)

Hive based automated jobs by Analytics at Wikimedia Deutschland. For our Airflow DAGs, please see [data-engineering/airflow-dags/wmde](https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/tree/main/wmde).

### Contents

- [airflow_jobs](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/hql/airflow_jobs)
  - All HQL jobs associated with [WMDE Airflow processes](https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/tree/main/wmde)
