-- Parameters:
--     <none>
--
-- Usage:
--     spark3-sql -f create_wd_item_sitelink_segments_weekly_table.hql    \
--         --database wmde                                                \
--         -d location=/wmf/data/wmde/wd_item_sitelink_segments_weekly

CREATE EXTERNAL TABLE IF NOT EXISTS `wd_item_sitelink_segments_weekly`(
    `week`                   date    COMMENT 'The date at the start of the week for which the segments are computed',
    `sitelink_items`         bigint  COMMENT 'Items that have a sitelink to one or more Wikimedia projects',
    `sitelink_item_targets`  bigint  COMMENT 'Items that those in sitelink_items are connected to',
    `all_other_items`        bigint  COMMENT 'Items not included in sitelink_items or sitelink_item_targets'
)

USING ICEBERG

TBLPROPERTIES (
    'format-version' = '2',
    'write.delete.mode' = 'copy-on-write',
    'write.parquet.compression-codec' = 'zstd'
)

LOCATION '${location}'
;
