-- Parameters:
--     source_table         -- Fully qualified table name to compute the
--                             aggregations for.
--     destination_table    -- Fully qualified table name to fill in
--                             aggregated values.
--     year                 -- Year of partition to compute aggregation
--                             for.
--     month                -- Month of partition to compute aggregation
--                             for.
--     day                  -- Day of partition to compute aggregation
--                             for.
--
-- Usage:
--     hive -f wd_item_sitelink_segments_weekly.hql                      \
--         -d source_table=wmf.wikidata_entity                           \
--         -d destination_table=wmde.wd_item_sitelink_segments_weekly    \
--         -d year=2024                                                  \
--         -d month=4                                                    \
--         -d day=1

-- Delete existing data for the period to prevent duplication of data in case of recomputation.
DELETE FROM
    ${destination_table}

WHERE
    week = CAST(
        CONCAT(
            LPAD(${year}, 4, '0'), '-',
            LPAD(${month}, 2, '0'), '-',
            LPAD(${day}, 2, '0')
        ) AS DATE
    )
;

WITH item_links AS (
    SELECT DISTINCT
        id AS item_id,
        siteLinks AS sitelinks,
        get_json_object(c.exploded_claims['mainSnak'].dataValue.value, '$.id') AS linked_item_id

    FROM
        ${source_table}

    LATERAL VIEW
        explode(claims) c AS exploded_claims

    WHERE
        snapshot = CONCAT(
            LPAD(${year}, 4, '0'), '-',
            LPAD(${month}, 2, '0'), '-',
            LPAD(${day}, 2, '0')
        )
        AND c.exploded_claims['mainSnak'].dataType = 'wikibase-item'
),

items_and_sitelinks AS (
    SELECT DISTINCT
        id AS item_id,
        siteLinks AS sitelinks

    FROM
        ${source_table}

    WHERE
        snapshot = CONCAT(
            LPAD(${year}, 4, '0'), '-',
            LPAD(${month}, 2, '0'), '-',
            LPAD(${day}, 2, '0')
        )
),

sitelink_items AS (
    SELECT DISTINCT
        item_id AS item_id

    FROM
        items_and_sitelinks

    WHERE
        sitelinks IS NOT NULL
),

sitelink_item_targets AS (
    SELECT DISTINCT
        l.linked_item_id AS item_id

    FROM
        item_links AS l

    LEFT JOIN
        items_and_sitelinks AS s

    ON
        l.linked_item_id = s.item_id

    WHERE
        l.sitelinks IS NOT NULL
        AND s.sitelinks IS NULL
)

INSERT INTO
    ${destination_table}

-- Spark hint to tell it to write only one file as an output for the job.
SELECT /*+ COALESCE(1) */
    CAST(
        CONCAT(
            LPAD(${year}, 4, '0'), '-',
            LPAD(${month}, 2, '0'), '-',
            LPAD(${day}, 2, '0')
        ) AS DATE
    ) AS week,
    count(DISTINCT s.item_id) AS sitelink_items,
    count(DISTINCT t.item_id) AS sitelink_item_targets,
    count(DISTINCT e.id) - count(DISTINCT s.item_id) - count(DISTINCT t.item_id) AS all_other_items

FROM
    ${source_table} AS e

LEFT JOIN
    sitelink_items AS s

ON
    e.id = s.item_id

LEFT JOIN
    sitelink_item_targets AS t

ON
    e.id = t.item_id

WHERE
    e.snapshot = CONCAT(
        LPAD(${year}, 4, '0'), '-',
        LPAD(${month}, 2, '0'), '-',
        LPAD(${day}, 2, '0')
    )
;
