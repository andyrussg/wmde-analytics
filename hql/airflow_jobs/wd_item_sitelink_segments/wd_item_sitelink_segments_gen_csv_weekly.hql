-- Parameters:
--     source_table       -- Fully qualified table name to compute the
--                           aggregations for.
--     destination_dir    -- Fully qualified directory name that the file
--                           should be exported to.
--
-- Usage:
--     spark3-sql -f wd_item_sitelink_segments_gen_csv_weekly.hql                     \
--         -d source_table=wmde.wd_item_sitelink_segments_weekly                      \
--         -d destination_dir=/tmp/wmde/analytics/wd_item_sitelink_segments_weekly

INSERT OVERWRITE DIRECTORY
    '${destination_dir}'

USING CSV OPTIONS
    ('sep'=',', 'header'='true', 'compression'='none')

SELECT /*+ COALESCE(1) */
    *

FROM
    ${source_table}
;
