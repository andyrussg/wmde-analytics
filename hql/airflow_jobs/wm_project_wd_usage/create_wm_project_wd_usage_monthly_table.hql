-- Creates the table statement for the wm_project_wd_usage_monthly table.
--
-- Parameters:
--     <none>
--
-- Usage:
--     spark3-sql -f create_wm_project_wd_usage_monthly_table.hql    \
--         --database wmde                                           \
--         -d location=/wmf/data/wmde/wm_project_wd_usage_monthly

CREATE EXTERNAL TABLE IF NOT EXISTS `wm_project_wd_usage_monthly`(
    `project_type`                   string  COMMENT 'The type of Wikimedia project (ex: Wikipedia)',
    `project`                        string  COMMENT 'The specific project of the project_type (ex: enwikipedia)',
    `total_articles`                 bigint  COMMENT 'The total articles (entries) that are found on the project',
    `total_use_wd_articles`          bigint  COMMENT 'The total articles (entries) on the project that use Wikidata',
    `percent_use_wd_articles`        bigint  COMMENT 'The percentage of articles (entries) on the project that use Wikidata',
    `total_has_sitelink_articles`    bigint  COMMENT 'The total articles (entries) on the project that use sitelinks',
    `percent_has_sitelink_articles`  bigint  COMMENT 'The percentage of articles (entries) on the project that use sitelinks'
)

USING ICEBERG

TBLPROPERTIES (
    'format-version' = '2',
    'write.delete.mode' = 'copy-on-write',
    'write.parquet.compression-codec' = 'zstd'
)

LOCATION '${location}'
;
