-- Run the queries in this file to test that the data process has finished successfully.
-- The test table will be dropped by the second query to assure that resources are respected.

-- Note: Please replace TEST_USER_DB for testing.

SELECT
    *

FROM
    TEST_USER_DB.wiktionary_cognate_missing_entries_daily

LIMIT
    5
;

-- Note: Save the output for your MR!

DROP TABLE
    TEST_USER_DB.wiktionary_cognate_missing_entries_daily
;
