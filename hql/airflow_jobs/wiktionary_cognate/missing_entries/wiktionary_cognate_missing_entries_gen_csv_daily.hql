-- Parameters:
--     source_table       -- Fully qualified table name to compute the
--                           aggregations for.
--     destination_dir    -- Fully qualified directory name that the file
--                           should be exported to.
--
-- Usage:
--     spark3-sql -f wiktionary_cognate_missing_entries_gen_csv_daily.hql                     \
--         -d source_table=wmde.wiktionary_cognate_missing_entries_daily                      \
--         -d destination_dir=/tmp/wmde/analytics/wiktionary_cognate_missing_entries_daily

INSERT OVERWRITE DIRECTORY
    '${destination_dir}'

USING CSV OPTIONS
    ('sep'=',', 'header'='true', 'compression'='none')

SELECT /*+ COALESCE(1) */
    *

FROM
    ${source_table}
;
