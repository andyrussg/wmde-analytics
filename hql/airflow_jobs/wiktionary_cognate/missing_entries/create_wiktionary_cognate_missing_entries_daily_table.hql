-- Parameters:
--     <none>
--
-- Usage:
--     spark3-sql -f create_wiktionary_cognate_missing_entries_daily_table.hql    \
--         --database wmde                                                        \
--         -d location=/wmf/data/wmde/wiktionary_cognate_missing_entries_daily

CREATE EXTERNAL TABLE IF NOT EXISTS `wiktionary_cognate_missing_entries_daily`(
    `wiktionary`                     string  COMMENT 'Wiktionary project identifiers to check entries against (iso2wiktionary)',
    `missing_entry`                  string  COMMENT 'Words that are missing from the project in the wiktionary column (top 1,000 based on total_wiktionaries_with_entry descending)',
    `total_wiktionaries_with_entry`  bigint  COMMENT 'The sum of all other Wiktionaries that contain the word in the missing_entry column',
)

USING ICEBERG

TBLPROPERTIES (
    'format-version' = '2',
    'write.delete.mode' = 'copy-on-write',
    'write.parquet.compression-codec' = 'zstd'
)

LOCATION '${location}'
;
