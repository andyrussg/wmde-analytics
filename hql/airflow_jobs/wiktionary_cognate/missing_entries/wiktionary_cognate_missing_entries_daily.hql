-- Parameters:
--     src_hive_wiktionary_cognate_sites     -- Fully qualified table get the project
--                                              entry relationships from.
--     src_hive_wiktionary_cognate_pages     -- Fully qualified table get the project
--                                              entry relationships from.
--     src_hive_wiktionary_cognate_titles    -- Fully qualified table get the project
--                                              entry relationships from.
--     destination_table                     -- Fully qualified table name to fill in
--                                              project entry relationships.
--     year                                  -- Year of partition to get the project
--                                              entry relationships from.
--     month                                 -- Month of partition to get the project
--                                              entry relationships from.
--     day                                   -- Day of partition to get the project
--                                              entry relationships from.
--
-- Usage:
--     hive -f wiktionary_cognate_missing_entries_daily.hql                        \
--         -d src_hive_wiktionary_cognate_sites=wmde.wiktionary_cognate_sites      \
--         -d src_hive_wiktionary_cognate_pages=wmde.wiktionary_cognate_pages      \
--         -d src_hive_wiktionary_cognate_titles=wmde.wiktionary_cognate_titles    \
--         -d destination_table=wmde.wiktionary_cognate_missing_entries_daily      \
--         -d year=2024                                                            \
--         -d month=6                                                              \
--         -d day=1

DELETE FROM
    ${destination_table}

WHERE
    1=1
;

WITH potential_wiktionary_entries AS (
    SELECT
        s.cgsi_dbname AS cgsi_dbname,
        t.cgti_normalized_key AS cgti_normalized_key

    FROM
        ${src_hive_wiktionary_cognate_sites} AS s

    CROSS JOIN
        ${src_hive_wiktionary_cognate_titles} AS t

    WHERE
        s.year = ${year}
        AND s.month = ${month}
        AND s.day = ${day}
        AND t.year = ${year}
        AND t.month = ${month}
        AND t.day = ${day}
),

actual_wiktionary_entries AS (
    SELECT DISTINCT
        s.cgsi_dbname AS cgsi_dbname,
        t.cgti_normalized_key AS cgti_normalized_key

    FROM
        ${src_hive_wiktionary_cognate_sites} AS s

    JOIN
        ${src_hive_wiktionary_cognate_pages} AS p

    ON
        s.cgsi_key = p.cgpa_site

    JOIN
        ${src_hive_wiktionary_cognate_titles} AS t

    ON
        p.cgpa_title = t.cgti_raw_key

    WHERE
        s.year = ${year}
        AND s.month = ${month}
        AND s.day = ${day}
        AND p.year = ${year}
        AND p.month = ${month}
        AND p.day = ${day}
        AND t.year = ${year}
        AND t.month = ${month}
        AND t.day = ${day}
),

wiktionary_missing_keys AS (
    SELECT
        pe.cgsi_dbname AS cgsi_dbname,
        pe.cgti_normalized_key AS cgti_normalized_key

    FROM
        potential_wiktionary_entries AS pe

    WHERE
        NOT EXISTS (
            SELECT
                1

            FROM
                actual_wiktionary_entries AS ae

            WHERE
                ae.cgti_normalized_key = pe.cgti_normalized_key
                AND ae.cgsi_dbname = pe.cgsi_dbname
        )
),

cgti_normalized_key_counts AS (
    SELECT
        cgti_normalized_key AS cgti_normalized_key,
        COUNT(*) AS total_cgti_normalized_key

    FROM
        actual_wiktionary_entries

    GROUP BY
        cgti_normalized_key
),

wiktionary_missing_key_counts AS (
    SELECT
        mk.cgsi_dbname AS cgsi_dbname,
        mk.cgti_normalized_key AS cgti_normalized_key,
        kc.total_cgti_normalized_key AS total_wiktionaries_with_entry

    FROM
        wiktionary_missing_keys AS mk

    JOIN
        cgti_normalized_key_counts AS kc

    ON
        mk.cgti_normalized_key = kc.cgti_normalized_key
),

wiktionary_missing_key_counts_ranked AS (
    SELECT
        cgsi_dbname AS cgsi_dbname,
        cgti_normalized_key AS cgti_normalized_key,
        total_wiktionaries_with_entry AS total_wiktionaries_with_entry,
        ROW_NUMBER() OVER (
            PARTITION BY
                cgsi_dbname

            ORDER BY
                total_wiktionaries_with_entry DESC
        ) AS rank

    FROM
        wiktionary_missing_key_counts

    WHERE
        total_wiktionaries_with_entry > 0
)

INSERT INTO
    ${destination_table}

SELECT
    r.cgsi_dbname AS wiktionary,
    t.cgti_raw AS missing_entry,
    r.total_wiktionaries_with_entry AS total_wiktionaries_with_entry

FROM
    wiktionary_missing_key_counts_ranked AS r

LEFT JOIN
    ${src_hive_wiktionary_cognate_titles} AS t

ON
    r.cgti_normalized_key = t.cgti_normalized_key

WHERE
    r.rank >= 1000
    AND t.year = ${year}
    AND t.month = ${month}
    AND t.day = ${day}

ORDER BY
    wiktionary ASC,
    total_wiktionaries_with_entry DESC,
    missing_entry ASC
;
