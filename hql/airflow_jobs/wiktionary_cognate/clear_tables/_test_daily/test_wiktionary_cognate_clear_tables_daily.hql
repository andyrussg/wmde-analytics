-- Parameters:
--     hive_wiktionary_cognate_sites       -- Fully qualified table get the project
--                                            entry relationships from.
--     hive_wiktionary_cognate_pages       -- Fully qualified table get the entry
--                                            entry relationships from.
--     hive_wiktionary_cognate_titles      -- Fully qualified table get the entry
--                                            entry relationships from.
--
-- Usage:
--     hive -f wiktionary_cognate_clear_tables_daily.hql                       \
--         -d hive_wiktionary_cognate_sites=wmde.wiktionary_cognate_sites      \
--         -d hive_wiktionary_cognate_pages=wmde.wiktionary_cognate_pages      \
--         -d hive_wiktionary_cognate_titles=wmde.wiktionary_cognate_titles    \
--         -d year=2024                                                        \
--         -d month=6                                                          \
--         -d day=1

-- Note: Please replace TEST_USER_DB for testing.
-- Note: We're cleaning data only past one week old to assure that backups are available.

DELETE FROM
    ${TEST_USER_DB.wiktionary_cognate_sites}

WHERE
    year = YEAR(DATE_SUB(CONCAT(${year}, '-', ${month}, '-', ${day}), INTERVAL 7 DAY))
    AND month = MONTH(DATE_SUB(CONCAT(${year}, '-', ${month}, '-', ${day}), INTERVAL 7 DAY))
    AND day = DAY(DATE_SUB(CONCAT(${year}, '-', ${month}, '-', ${day}), INTERVAL 7 DAY));
;

DELETE FROM
    ${TEST_USER_DB.wiktionary_cognate_pages}

WHERE
    year = YEAR(DATE_SUB(CONCAT(${year}, '-', ${month}, '-', ${day}), INTERVAL 7 DAY))
    AND month = MONTH(DATE_SUB(CONCAT(${year}, '-', ${month}, '-', ${day}), INTERVAL 7 DAY))
    AND day = DAY(DATE_SUB(CONCAT(${year}, '-', ${month}, '-', ${day}), INTERVAL 7 DAY));
;

DELETE FROM
    ${TEST_USER_DB.wiktionary_cognate_titles}

WHERE
    year = YEAR(DATE_SUB(CONCAT(${year}, '-', ${month}, '-', ${day}), INTERVAL 7 DAY))
    AND month = MONTH(DATE_SUB(CONCAT(${year}, '-', ${month}, '-', ${day}), INTERVAL 7 DAY))
    AND day = DAY(DATE_SUB(CONCAT(${year}, '-', ${month}, '-', ${day}), INTERVAL 7 DAY));
;
