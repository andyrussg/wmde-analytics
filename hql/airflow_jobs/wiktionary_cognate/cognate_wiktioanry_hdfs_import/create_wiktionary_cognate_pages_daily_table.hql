-- See: https://www.mediawiki.org/wiki/Extension:Cognate
--
-- Parameters:
--     <none>
--
-- Usage:
--     spark3-sql -f create_wiktionary_cognate_pages_daily_table.hql    \
--         --database wmde                                              \
--         -d location=/wmf/data/wmde/wiktionary_cognate_pages_daily

CREATE EXTERNAL TABLE IF NOT EXISTS `wiktionary_cognate_pages_daily`(
    `cgpa_site`          string  COMMENT 'The wiktionary project key of the given page - links to cognate_sites.cgsi_key',
    `cgpa_namespace`     string  COMMENT 'The namespace of the given page',
    `cgpa_title`         string  COMMENT 'The title key of the given page - links to cognate_titles.cgti_raw_key',
    `year`               bigint  COMMENT 'Unpadded year of request (partition field)',
    `month`              bigint  COMMENT 'Unpadded month of request (partition field)',
    `day`                bigint  COMMENT 'Unpadded day of request (partition field)'
)

USING ICEBERG

TBLPROPERTIES (
    'format-version' = '2',
    'write.delete.mode' = 'copy-on-write',
    'write.parquet.compression-codec' = 'zstd'
)

LOCATION '${location}'
;
