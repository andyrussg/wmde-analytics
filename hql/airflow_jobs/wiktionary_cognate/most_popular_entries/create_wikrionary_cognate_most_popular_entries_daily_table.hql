-- Parameters:
--     <none>
--
-- Usage:
--     spark3-sql -f create_wikrionary_cognate_most_popular_entries_daily_table.hql    \
--         --database wmde                                                             \
--         -d location=/wmf/data/wmde/wikrionary_cognate_most_popular_entries_daily

CREATE EXTERNAL TABLE IF NOT EXISTS `wikrionary_cognate_most_popular_entries_daily`(
    `entry`                          string  COMMENT 'A word that appears in at least 10 Wiktionaries',
    `total_wiktionaries_with_entry`  bigint  COMMENT 'The sum of all Wiktionaries that contain the word in the entry column',
)

USING ICEBERG

TBLPROPERTIES (
    'format-version' = '2',
    'write.delete.mode' = 'copy-on-write',
    'write.parquet.compression-codec' = 'zstd'
)

LOCATION '${location}'
;
