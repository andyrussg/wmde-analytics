-- Parameters:
--     src_hive_wiktionary_cognate_sites     -- Fully qualified table get the project
--                                              entry relationships from.
--     src_hive_wiktionary_cognate_pages     -- Fully qualified table get the project
--                                              entry relationships from.
--     src_hive_wiktionary_cognate_titles    -- Fully qualified table get the project
--                                              entry relationships from.
--     destination_table                     -- Fully qualified table name to fill in
--                                              project entry relationships.
--     year                                  -- Year of partition to get the project
--                                              entry relationships from.
--     month                                 -- Month of partition to get the project
--                                              entry relationships from.
--     day                                   -- Day of partition to get the project
--                                              entry relationships from.
--
-- Usage:
--     hive -f wiktionary_cognate_most_popular_entries_daily.hql                      \
--         -d src_hive_wiktionary_cognate_sites=wmde.wiktionary_cognate_sites         \
--         -d src_hive_wiktionary_cognate_pages=wmde.wiktionary_cognate_pages         \
--         -d src_hive_wiktionary_cognate_titles=wmde.wiktionary_cognate_titles       \
--         -d destination_table=wmde.wiktionary_cognate_most_popular_entries_daily    \
--         -d year=2024                                                               \
--         -d month=6                                                                 \
--         -d day=1

-- Note: Please replace TEST_USER_DB, TEST_YEAR, TEST_MONTH and TEST_DAY for testing.

DELETE FROM
    TEST_USER_DB.wiktionary_cognate_most_popular_entries_daily

WHERE
    1=1
;

WITH wiktionary_entries AS (
    SELECT DISTINCT
        s.cgsi_dbname AS cgsi_dbname,
        t.cgti_normalized_key AS cgti_normalized_key

    FROM
        wmde.wiktionary_cognate_sites AS s

    JOIN
        wmde.wiktionary_cognate_pages AS p

    ON
        s.cgsi_key = p.cgpa_site

    JOIN
        wmde.wiktionary_cognate_title AS t

    ON
        p.cgpa_title = t.cgti_raw_key

    WHERE
        s.year = TEST_YEAR
        AND s.month = TEST_MONTH
        AND s.day = TEST_DAY
        AND p.year = TEST_YEAR
        AND p.month = TEST_MONTH
        AND p.day = TEST_DAY
        AND t.year = TEST_YEAR
        AND t.month = TEST_MONTH
        AND t.day = TEST_DAY
)

INSERT INTO
    TEST_USER_DB.wiktionary_cognate_most_popular_entries_daily

SELECT
    t.cgti_raw AS entry,
    count(e.cgti_normalized_key) AS total_wiktionaries_with_entry

FROM
    wiktionary_entries AS e

LEFT JOIN
    wmde.wiktionary_cognate_title AS t

ON
    e.cgti_normalized_key = t.cgti_normalized_key

WHERE
    t.year = TEST_YEAR
    AND t.month = TEST_MONTH
    AND t.day = TEST_DAY

ORDER BY
    total_wiktionaries_with_entry DESC,
    entry ASC

LIMIT
    10000
;
