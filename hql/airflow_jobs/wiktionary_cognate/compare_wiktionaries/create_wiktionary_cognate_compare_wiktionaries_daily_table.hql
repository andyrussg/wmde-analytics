-- Parameters:
--     <none>
--
-- Usage:
--     spark3-sql -f create_wiktionary_cognate_compare_wiktionaries_daily_table.hql    \
--         --database wmde                                                             \
--         -d location=/wmf/data/wmde/wiktionary_cognate_compare_wiktionaries_daily

CREATE EXTERNAL TABLE IF NOT EXISTS `wiktionary_cognate_compare_wiktionaries_daily`(
    `source_wiktionary`     string  COMMENT 'Wiktionary project identifiers to compare entries from (iso2wiktionary)',
    `target_wiktionary`     string  COMMENT 'Wiktionary project identifiers to compare entries to (iso2wiktionary)',
    `missing_source_entry`  string  COMMENT 'Wiktionary entries that are within the Wiktionary of the target_wiktionary column but not in the Wiktionary of the source_wiktionary column',
)

USING ICEBERG

TBLPROPERTIES (
    'format-version' = '2',
    'write.delete.mode' = 'copy-on-write',
    'write.parquet.compression-codec' = 'zstd'
)

LOCATION '${location}'
;
