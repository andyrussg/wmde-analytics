-- Parameters:
--     src_hive_wiktionary_cognate_sites     -- Fully qualified table get the project
--                                              entry relationships from.
--     src_hive_wiktionary_cognate_pages     -- Fully qualified table get the project
--                                              entry relationships from.
--     src_hive_wiktionary_cognate_titles    -- Fully qualified table get the project
--                                              entry relationships from.
--     destination_table                     -- Fully qualified table name to fill in
--                                              project entry relationships.
--     year                                  -- Year of partition to get the project
--                                              entry relationships from.
--     month                                 -- Month of partition to get the project
--                                              entry relationships from.
--     day                                   -- Day of partition to get the project
--                                              entry relationships from.
--
-- Usage:
--     hive -f wiktionary_cognate_compare_wiktionaries_daily.hql                      \
--         -d src_hive_wiktionary_cognate_sites=wmde.wiktionary_cognate_sites         \
--         -d src_hive_wiktionary_cognate_pages=wmde.wiktionary_cognate_pages         \
--         -d src_hive_wiktionary_cognate_titles=wmde.wiktionary_cognate_titles       \
--         -d destination_table=wmde.wiktionary_cognate_compare_wiktionaries_daily    \
--         -d year=2024                                                               \
--         -d month=6                                                                 \
--         -d day=1

DELETE FROM
    ${destination_table}

WHERE
    1=1
;

WITH wiktionary_entries AS (
    SELECT DISTINCT
        s.cgsi_dbname AS wiktionary,
        t.cgti_normalized_key AS cgti_normalized_key

    FROM
        ${src_hive_wiktionary_cognate_sites} AS s

    JOIN
        ${src_hive_wiktionary_cognate_pages} AS p

    ON
        s.cgsi_key = p.cgpa_site

    JOIN
        ${src_hive_wiktionary_cognate_titles} AS t

    ON
        p.cgpa_title = t.cgti_raw_key

    WHERE
        s.year = ${year}
        AND s.month = ${month}
        AND s.day = ${day}
        AND p.year = ${year}
        AND p.month = ${month}
        AND p.day = ${day}
        AND t.year = ${year}
        AND t.month = ${month}
        AND t.day = ${day}
)

INSERT INTO
    ${destination_table}

SELECT
    e2.wiktionary AS source_wiktionary,
    e1.wiktionary AS target_wiktionary,
    t.cgti_raw AS missing_source_entry

FROM
    wiktionary_entries AS e1

LEFT JOIN
    wiktionary_entries AS e2

ON
    e1.cgti_normalized_key = e2.cgti_normalized_key
    AND e1.wiktionary != e2.wiktionary

LEFT JOIN
    ${src_hive_wiktionary_cognate_titles} AS t

ON
    e1.cgti_normalized_key = t.cgti_normalized_key

WHERE
    e2.cgti_normalized_key IS NULL
    AND t.year = ${year}
    AND t.month = ${month}
    AND t.day = ${day}

ORDER BY
    source_wiktionary ASC,
    target_wiktionary ASC,
    missing_source_entry ASC
;
