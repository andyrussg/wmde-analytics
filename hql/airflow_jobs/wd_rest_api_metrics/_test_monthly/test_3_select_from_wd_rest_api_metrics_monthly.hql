-- Parameters:
--     source_table    -- Fully qualified table name to compute the
--                        aggregations for.
--
-- Usage:
--     hive -f select_from_wd_rest_api_metrics_monthly.hql     \
--         -d source_table=wmde.wd_rest_api_metrics_monthly

-- Note: Please replace TEST_USER_DB for testing.

SELECT
    month AS month,
    CASE
        WHEN
            total_user_agents >= 25
        THEN
            cast(total_user_agents AS STRING)
        ELSE
            '<25'
    END AS total_user_agents,
    CASE
        WHEN
            total_filtered_user_agents >= 25
        THEN
            cast(total_filtered_user_agents AS STRING)
        ELSE
            '<25'
    END AS total_filtered_user_agents,
    CASE
        WHEN
            total_ips >= 25
        THEN
            cast(total_ips AS STRING)
        ELSE
            '<25'
    END AS total_ips

FROM
    TEST_USER_DB.wd_rest_api_metrics_monthly
;
