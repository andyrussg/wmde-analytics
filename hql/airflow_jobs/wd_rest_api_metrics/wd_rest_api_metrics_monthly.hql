-- Parameters:
--     source_table         -- Fully qualified table name to compute the
--                             aggregations for.
--     destination_table    -- Fully qualified table name to fill in
--                             aggregated values.
--     year                 -- Year of partition to compute aggregation
--                             for.
--     month                -- Month of partition to compute aggregation
--                             for.
--
-- Usage:
--     hive -f wd_rest_api_metrics_monthly.hql                      \
--         -d source_table=wmf.webrequest                           \
--         -d destination_table=wmde.wd_rest_api_metrics_monthly    \
--         -d year=2024                                             \
--         -d month=2

-- Delete existing data for the period to prevent duplication of data in case of recomputation.
DELETE FROM
    ${destination_table}

WHERE
    month = CAST(
        CONCAT(
            LPAD(${year}, 4, '0'), '-',
            LPAD(${month}, 2, '0'), '-',
            '01'
        ) AS DATE
    )
;

INSERT INTO
    ${destination_table}

-- Spark hint to tell it to write only one file as an output for the job.
SELECT /*+ COALESCE(1) */
    CAST(
        CONCAT(
            LPAD(${year}, 4, '0'), '-',
            LPAD(${month}, 2, '0'), '-',
            '01'
        ) AS DATE
    ) AS month,
    count(DISTINCT user_agent) AS total_user_agents,
    count(
        DISTINCT CASE
            WHEN user_agent
            NOT LIKE 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/% (KHTML, like Gecko) Chrome/% Safari/%'
            THEN user_agent
        END
    ) AS total_filtered_user_agents,
    count(DISTINCT ip) AS total_ips

FROM
    ${source_table}

WHERE
    year = ${year}
    AND month = ${month}
    AND webrequest_source = 'text'
    AND uri_host = 'www.wikidata.org'
    AND uri_path LIKE '/w/rest.php/wikibase/%'
;
