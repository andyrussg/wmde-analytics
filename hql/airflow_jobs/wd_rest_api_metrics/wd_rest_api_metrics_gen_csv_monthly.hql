-- Parameters:
--     source_table       -- Fully qualified table name to compute the
--                           aggregations for.
--     destination_dir    -- Fully qualified directory name that the file
--                           should be exported to.
--
-- Usage:
--     spark3-sql -f wd_rest_api_metrics_gen_csv_monthly.hql                     \
--         -d source_table=wmde.wd_rest_api_metrics_monthly                      \
--         -d destination_dir=/tmp/wmde/analytics/wd_rest_api_metrics_monthly

INSERT OVERWRITE DIRECTORY
    '${destination_dir}'

USING CSV OPTIONS
    ('sep'=',', 'header'='true', 'compression'='none')

SELECT /*+ COALESCE(1) */
    month AS month,
    CASE
        WHEN
            total_user_agents >= 25
        THEN
            cast(total_user_agents AS STRING)
        ELSE
            '<25'
    END AS total_user_agents,
    CASE
        WHEN
            total_filtered_user_agents >= 25
        THEN
            cast(total_filtered_user_agents AS STRING)
        ELSE
            '<25'
    END AS total_filtered_user_agents,
    CASE
        WHEN
            total_ips >= 25
        THEN
            cast(total_ips AS STRING)
        ELSE
            '<25'
    END AS total_ips

FROM
    ${source_table}
;
