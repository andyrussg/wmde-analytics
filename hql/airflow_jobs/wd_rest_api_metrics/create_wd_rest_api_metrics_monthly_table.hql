-- Parameters:
--     <none>
--
-- Usage:
--     spark3-sql -f create_wd_rest_api_metrics_monthly_table.hql    \
--         --database wmde                                           \
--         -d location=/wmf/data/wmde/wd_rest_api_metrics_monthly

CREATE EXTERNAL TABLE IF NOT EXISTS `wd_rest_api_metrics_monthly`(
    `month`                       date    COMMENT 'The month for which the metrics are computed over',
    `total_user_agents`           bigint  COMMENT 'Total distinct user agents making REST API requests in the given month',
    `total_filtered_user_agents`  bigint  COMMENT 'Total distinct user agents making REST API requests in the given month filtered for user agents found to have outlier activity',
    `total_ips`                   bigint  COMMENT 'Total distinct IPs making REST API requests in the given month'
)

USING ICEBERG

TBLPROPERTIES (
    'format-version' = '2',
    'write.delete.mode' = 'copy-on-write',
    'write.parquet.compression-codec' = 'zstd'
)

LOCATION '${location}'
;
