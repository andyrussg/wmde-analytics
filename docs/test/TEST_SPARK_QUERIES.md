# [Spark Query Testing](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/docs/test/TEST_SPARK_QUERIES.md)

> Testing HiveQL queries within a personal database on the WMF analytics cluster/infrastructure.

Sources:

- [wikitech/Analytics/Systems/Cluster/Spark](https://wikitech.wikimedia.org/wiki/Analytics/Systems/Cluster/Spark)

### Access Spark SQL Server

First SSH into your stats server of choice and authenticate with Kerberos if it’s not cached:

```bash
ssh statSTAT_NUM.eqiad.wmnet
kinit  # if needed, then enter your Kerberos password
```

Then do a command from the [Spark Shell Wikitech Documentation](https://wikitech.wikimedia.org/wiki/Analytics/Systems/Cluster/Spark#Start_a_spark_shell_in_yarn) such as:

```bash
spark3-sql --master yarn --executor-memory 8G --executor-cores 4 --driver-memory 2G --conf spark.dynamicAllocation.maxExecutors=64
```

### Run Test Queries

You can now execute SQL statements against the database:

```sql
USE USER_DB;
SHOW TABLES;
```
