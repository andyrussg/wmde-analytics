# [Airflow Dev Instance Testing](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/docs/test/TEST_AIRFLOW_DEV_INSTANCE.md)

> Testing Airflow DAGs and jobs via an Airflow instance deployed on the WMF analytics cluster/infrastructure.

Sources:

- [wikitech/Data_Engineering/Systems/Airflow/Developer_guide#Development_instance](https://wikitech.wikimedia.org/wiki/Data_Engineering/Systems/Airflow/Developer_guide#Development_instance)

### Setup Airflow Deployment

First SSH into your stats server of choice and authenticate with Kerberos if it’s not cached:

```bash
ssh statSTAT_NUM.eqiad.wmnet
kinit  # if needed, then enter your Kerberos password
```

From here note that you'll need to be deploying a copy of [data-engineering/airflow-dags](https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags) that has the DAGs you'd like to test. This could be your fork if you chose to make one, or for those working directly via branch commits to the upstream repo you would clone the repo itself and switch to your branch.

```bash
# Via a fork:
git clone https://gitlab.wikimedia.org/YOUR_USERNAME/airflow-dags.git

# Via upstream:
git clone https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags.git

# Then enter your DAG repo and checkout the branch with the DAG you want to test:
cd airflow-dags
git branch -a  # to get your branch name
git checkout YOUR_BRANCH_NAME
```

### Run Test Deployment

Run the following to create a virtual environment and an SQLite DB as well as launch the `wmde` Airflow processes:

```bash
# Orient yourself to the CLI options:
./run_dev_instance.sh -h

./run_dev_instance.sh wmde
```

### Test DAG

Once everything's ready, open an SSH tunnel locally from a new terminal window to access the Airflow UI as described in the script logs. You'll then need to edit the variables for the DAG via the following steps:

- Select `Admin`
- Select `Variables`
- Select `Show record` to view the variables or `Edit record` to change them
- Change the variables to access tables in your local DB and export to your local file system on the given server

You can now unpause the DAG and check that the outputs are expected. If so, then you're ready for a merge request to [data-engineering/airflow-dags](https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags).
