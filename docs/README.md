# [Docs](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/docs)

Docs for regular processes by Analytics at Wikimedia Deutschland.

### Contents

- [Test](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/docs/test)
  - Steps for testing infrastructure
