# [Analytics](https://gitlab.wikimedia.org/repos/wmde/analytics)

Research, tasks and automated jobs by Analytics at Wikimedia Deutschland. For our Airflow DAGs, please see [data-engineering/airflow-dags/wmde](https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/tree/main/wmde).

### Contents

- [Docs](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/docs)
  - Docs for regular processes
- [HQL](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/hql)
  - Hive based automated jobs
- [Spark](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/spark)
  - Spark based automated jobs
- [Tasks](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/tasks)
  - Research and tasks
- [UDFs](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/udfs)
  - User defined functions

## Setup

### [conda env](https://conda.io/projects/conda/en/latest/user-guide/getting-started.html)

```bash
conda env create -f conda-environment.yaml
conda activate wmde-analytics
```

### [pre-commit](https://pre-commit.com/)

```bash
pre-commit install
# To run:
pre-commit run --all-files
```
