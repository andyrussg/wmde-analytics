# [Spark](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/spark)

Spark based automated jobs by Analytics at Wikimedia Deutschland. For our Airflow DAGs, please see [data-engineering/airflow-dags/wmde](https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/tree/main/wmde).

### Contents

- [airflow_jobs](https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/spark/airflow_jobs)
  - All Spark jobs associated with [WMDE Airflow processes](https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/tree/main/wmde)
